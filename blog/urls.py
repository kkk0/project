from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('popular/', views.popular, name="popular"),
    path('news/', views.news, name="news"),
    path('search/', views.search_results, name='search_result'),
    path('post/<str:slug>/', views.post_detail, name="post_detail_url"),
    path('post/<str:slug>/leave-comment', views.leave_comment, name='leave_comment'),
    path('category/<slug:slug>/', views.category_detail, name="category_detail_url"),
    path('register/', views.register, name="register"),
    path('contact/', views.contact, name="contact"),
]