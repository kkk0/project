from django.shortcuts import render, redirect
from django.db.models import Q
from .models import Post, Category, Comment, Contact
from .forms import RegisterForm
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.urls import reverse


def index(request):
    news = Post.objects.order_by('-date')[:3]
    popular = Post.objects.filter(views__gt = 10)[:3]
    return render(request, 'blog/index.html', {'news':news, 'popular': popular})


def popular(request):
    popular = Post.objects.filter(views__gt = 10)
    return render(request, 'blog/popular.html', {'popular':popular})

def news(request):
    news = Post.objects.order_by('-date')
    return render(request, 'blog/news.html', {'news':news})
    
def category(request):
    category = Post.objects.order_by('-date')
    return render(request, 'blog/category.html', {'category':category})

def search_results(request):
    query = request.GET.get('search')
    search_obj = Post.objects.filter(
        Q(title__icontains = query) | Q(summary__icontains = query)
    )
    return render(request, 'blog/search_result.html', {'query':query, 'search_obj':search_obj})

def post_detail(request, slug):
    post = Post.objects.get(slug__iexact = slug)
    post.views += 1
    post.save()
    return render(request, 'blog/post_detail.html', {'post':post})

def category_detail(request, slug):
    category = Category.objects.get(slug__iexact = slug)
    posts = Post.objects.order_by('-date')
    return render(request, 'blog/category_detail.html', {'category':category, 'posts':posts})

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = RegisterForm()
    return render(request, 'blog/register.html', {'form':form})

def logout_views(request):
    logout(request)
    return redirect('index')

def leave_comment(request, slug):
    post = Post.objects.get(slug__iexact=slug)
    if  request.user.is_authenticated:
        user = request.user.first_name
        post.comment_set.create(
            author_name = user,
            text = request.POST.get('text')
        )
    else:
        post.comment_set.create(
            author_name = request.POST.get('name'), 
            text = request.POST.get('text')
    )
    return redirect(reverse('post_detail_url', args = (slug,)))


def contact(request):
    if request.method == 'POST':
        Contact.objects.create(
            name = request.POST.get('name'),
            last_name = request.POST.get('last_name'),
            email = request.POST.get('email'),
            phone = request.POST.get('phone'),
            message = request.POST.get('text')
        )
    return render(request, 'blog/contact.html')