from django.db import models
from django.utils import timezone 
from django.shortcuts import reverse

class Category(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    slug = models.SlugField('Ссылка', max_length=255, unique=True)
    image = models.ImageField('Картинка', blank=True)

    def get_absolute_url(self):
       return reverse('category_detail_url', kwargs={'slug':self.slug})

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):  
        return self.title

class Post(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    slug = models.SlugField('Ссылка', max_length=255, unique=True)
    summary = models.TextField('Краткое описание')
    content = models.TextField('Описание')
    date = models.DateTimeField('Дата', default=timezone.now)
    image = models.ImageField('Картинка', blank=True)
    categories = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)
    views = models.IntegerField('Просмотры', default=0)

    def get_absolute_url(self):
       return reverse('post_detail_url', kwargs={'slug':self.slug})

    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'

    def __str__(self):  
        return self.title

class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    author_name = models.CharField('ИМя автора', max_length=255)
    text = models.TextField('Текст комментария')
    date = models.DateTimeField('Дата коментария', default=timezone.now)

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"

    def __str__(self):
        return self.author_name

class Contact(models.Model):
    name = models.CharField("Имя", max_length=255)
    last_name = models.CharField("Фамилия", max_length=255)
    email = models.EmailField("Емаил")
    phone = models.CharField("Телефон", max_length=255)
    message = models.TextField('Текст сообщения')
    date = models.DateTimeField("Дата", default=timezone.now)

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"

    def __str__(self):
        return self.name
