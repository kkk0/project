from django.contrib import admin
from .models import Post, Category, Comment, Contact

class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug':("title",)}

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug':("title",)}

admin.site.register(Category, CategoryAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Comment)
admin.site.register(Contact)